from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from src.post.models import Post, PostLike
from src.user.serializer import CustomUserSerializer


class CurrentUserDefault:
    requires_context = True

    def __call__(self, serializer_field):
        return serializer_field.context['request'].user


class PostLikeSerializer(serializers.ModelSerializer):
    user = CustomUserSerializer(read_only=True, default=CurrentUserDefault())
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all())

    def validate(self, attrs):
        if self.context.get('request'):
            user = self.context.get('request').user
            if user == attrs.get('post').user:
                raise serializers.ValidationError('User cannot like their own posts')
        return attrs

    class Meta:
        model = PostLike
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=PostLike.objects.all(),
                fields=['post', 'user']
            )]


class PostSerializer(serializers.ModelSerializer):
    user = CustomUserSerializer(read_only=True)
    likes = serializers.SerializerMethodField()
    unlikes = serializers.SerializerMethodField()

    def get_likes(self, obj):
        return obj.postlike_set.filter(like=True).count()

    def get_unlikes(self, obj):
        return obj.postlike_set.filter(like=False).count()

    class Meta:
        model = Post
        fields = '__all__'
from django.urls import path
from rest_framework import routers

from src.post.views import PostListCreateView, PostLikeCreateView

router = routers.SimpleRouter()
router.register(r'like', PostLikeCreateView)
urlpatterns = router.urls

urlpatterns += [
    path('', PostListCreateView.as_view()),
]
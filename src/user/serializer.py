from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from src.user.models import CustomUser
from src.user.validators import HunterVerifier


class CustomUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=CustomUser.objects.all()), HunterVerifier()])
    password = serializers.CharField(write_only=True)
    first_name = serializers.ReadOnlyField()
    last_name = serializers.ReadOnlyField()

    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'email', 'password']

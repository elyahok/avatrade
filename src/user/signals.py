from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from src.user.models import CustomUser
import clearbit


@receiver(post_save, sender=CustomUser)
def my_handler(sender, created, instance, **kwargs):
    if created:
        clearbit.key = settings.CLEARBIT_SK_KEY
        person = clearbit.Person.find(email=instance.email, stream=True)
        if person:
            instance.first_name = person['name']['givenName']
            instance.last_name = person['name']['familyName']
            instance.save()

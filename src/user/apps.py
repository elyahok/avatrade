from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'src.user'

    def ready(self):
        from . import signals

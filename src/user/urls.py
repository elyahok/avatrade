from django.urls import path

from src.user.views import UserCreateView

urlpatterns = [
    path('signup', UserCreateView.as_view())
]
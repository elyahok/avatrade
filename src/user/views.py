from django.contrib.auth.hashers import make_password
from rest_framework import generics, permissions

from src.user.models import CustomUser
from src.user.serializer import CustomUserSerializer


class UserCreateView(generics.CreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
    permission_classes = [permissions.AllowAny]

    def perform_create(self, serializer):
        serializer.save(password=make_password(serializer.validated_data.get('password')))

from django.conf import settings
from pyhunter import PyHunter
from rest_framework import serializers


class HunterVerifier:
    hunter = PyHunter(settings.HUNTER_API_KEY)

    def __call__(self, value):
        if not (self.hunter.email_verifier(value).get('status')) in ['valid', 'webmail']:
            raise serializers.ValidationError('Enter a valid email')
